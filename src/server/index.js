var path = require("path");
var compression = require("compression");
const express = require("express");
const favicon = require("express-favicon");
const app = require("express")();
const PORT = process.env.PORT || 8080;
var approot = process.env.PWD;


app.use(compression());
app.use(express.static(approot));
app.use(favicon(approot + "/build/favicon.ico"));
app.use(express.static(path.join(approot, "build")));

app.get("/api/ping", function (req, res) {
    return res.send("pong");
});

app.get("/*", function (req, res) {
    res.sendFile(path.join(approot, "build", "index.html"));
});

app.listen(PORT)
